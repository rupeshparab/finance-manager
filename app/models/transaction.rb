class Transaction < ActiveRecord::Base
  belongs_to :user
  belongs_to :transable, polymorphic: true, :validate => true
  validates :amount, :date, presence: true
  validates_associated :transable

  def transable_type=(sType)
     super(sType.to_s.classify.constantize.base_class.to_s)
  end
end

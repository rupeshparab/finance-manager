class Category < ActiveRecord::Base
  belongs_to :user

  validates :source,  length: { in: 1..30 }

  validates :desc,    length: { in: 1..100 }, uniqueness: { scope: :source,
    message: "Same Source and Description can't be repeated" }

  validates :source, :desc, presence: true

  has_many :trans, foreign_key: "transable_id", class_name: "Transaction", as: :transable, dependent: :destroy, :validate => true

end

class WelcomeController < ApplicationController

  def index
    @trans = Transaction.all
    flash[:start_date] = @trans.first.date.strftime('%Y-%m-%d')
    flash[:end_date] = @trans.last.date.strftime('%Y-%m-%d')
  end

  def search
    start_date = Date.strptime(params[:start_date],'%Y-%m-%d')
    end_date = Date.strptime(params[:end_date],'%Y-%m-%d')
    if start_date > end_date
      temp = start_date
      start_date = end_date
      end_date = temp
    end
    if ActiveRecord::Base.connection.instance_values["config"][:adapter] == 'sqlite3'
      @trans = Transaction.where(:date => start_date..end_date.tomorrow)
    else
      @trans = Transaction.where(:date => start_date..end_date)
    end
    flash[:start_date] = start_date.strftime('%Y-%m-%d')
    flash[:end_date] = end_date.strftime('%Y-%m-%d')
    render "welcome/index"
  end

end

class AddController < ApplicationController
  def income
    @cat = IncomeCategory.all
    @type = 'IncomeCategory'
    render "transaction/form", date: "Hi"
  end

  def add
    cid = params[:category][:category_id] rescue nil
    source = params[:source] rescue nil
    desc = params[:desc] rescue nil
    type = params[:type] rescue nil
    date = params[:date] rescue nil
    amount = params[:amount] rescue nil
    flash[:cid] = cid
    flash[:source] = source
    flash[:desc] = desc
    flash[:type] = type
    flash[:date] = date
    flash[:amount] = amount
    if cid.nil? || cid==""
      @cat = Category.new({ source: source, desc: desc, type: type })
      @cat.user = current_user
      if !@cat.save
        return redirect_to :back, alert: @cat.errors
      end
    else
      @cat = Category.find(params[:category][:category_id]) rescue nil
      if @cat.nil?
        return redirect_to :back, alert: 'Please select proper category'
      end
    end

    @tran = Transaction.new({ date: date, amount: amount })
    if @cat.type == 'IncomeCategory'
      @tran.type = 'IncomeTransaction'
    else
      @tran.type = 'ExpenseTransaction'
    end
    @tran.transable = @cat

    if @tran.save
      redirect_to '/'
    else
      redirect_to :back, alert: @tran.errors, cat_selected: cid,
                          source: source, desc: desc,
                          date: date, amount: amount
    end

  end

  def expense
    @cat = ExpenseCategory.all
    @type = 'ExpenseCategory'
    render "transaction/form", date: "Hi"
  end

  def show
    @tran = Transaction.find(params[:id])
    flash[:date] = @tran.date.strftime('%Y-%m-%d')
    flash[:amount] = @tran.amount

    @cate = Category.find(@tran.transable_id)
    flash[:cid] = @cate.id
    flash[:source] = @cate.source
    flash[:desc] = @cate.desc

    @type = @cate.type
    @cat = Category.where(type: @type)
    render "transaction/edit"
  end

  def update
    cid = params[:category][:category_id] rescue nil
    source = params[:source] rescue nil
    desc = params[:desc] rescue nil
    type = params[:type] rescue nil
    date = params[:date] rescue nil
    amount = params[:amount] rescue nil
    if cid.nil? || cid==""
      @cat = Category.new({ source: source, desc: desc, type: type })
      @cat.user = current_user
      if !@cat.save
        return redirect_to :back, alert: @cat.errors, data: params
      end
    else
      @cat = Category.find(params[:category][:category_id]) rescue nil
      if @cat.nil?
        return redirect_to :back, alert: 'Please select proper category',
                            cat_selected: cid,
                            source: source, desc: desc,
                            date: date, amount: amount
      end
    end

    @tran = Transaction.find(params[:id])

    if @cat.type == 'IncomeCategory'
      temp_type = 'IncomeTransaction'
    else
      temp_type = 'ExpenseTransaction'
    end

    if @tran.update({ date: date, amount: amount, type: temp_type, transable: @cat })
      redirect_to :back, notice: 'Transaction Updated'
    else
      redirect_to :back, alert: @tran.errors, cat_selected: cid,
                          source: source, desc: desc,
                          date: date, amount: amount
    end
  end

  def delete
    Transaction.find(params[:id]).delete
    redirect_to '/', alert: 'Transaction Deleted'
  end
end

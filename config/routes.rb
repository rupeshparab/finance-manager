Rails.application.routes.draw do
  devise_for :users

  root to: 'welcome#index'

  get   'add/income'          => 'add#income',        as: :add_income
  get   'add/expense'         => 'add#expense',       as: :add_expense

  post  'transaction/search'  => 'welcome#search',    as: :tran_search

  post  'add'                 => 'add#add',           as: :add_save

  put   'transaction/:id'     => 'add#update',        as: :edit

  get   'transaction/:id'     => 'add#show',          as: :transaction
  delete 'transaction/:id'    => 'add#delete',        as: :transaction_delete

end

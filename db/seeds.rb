# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
user = User.create({ name: 'Rupesh Parab', email: 'user@user.com', password: 'password'})

cat = IncomeCategory.create({ source: 'ABC Pvt Ltd.', desc: 'Monthly Salary from company', user: user })

IncomeTransaction.create({ amount: '50000', date: Date.strptime("01/10/2016", "%m/%d/%Y") , transable: cat })

cat = IncomeCategory.create({ source: 'Freelance Corp.', desc: 'Amount recieved for XYZ project', user: user })

IncomeTransaction.create({ amount: '100000', date: Date.strptime("01/15/2016", "%m/%d/%Y") , transable: cat })

cat = ExpenseCategory.create({ source: 'PQR Restaurant', desc: 'Amount spent on food monthly', user: user })

ExpenseTransaction.create({ amount: '10000', date: Date.strptime("01/15/2016", "%m/%d/%Y") , transable: cat })

cat = ExpenseCategory.create({ source: 'Resort XYZ', desc: 'Amount spent for weekend trip', user: user })

ExpenseTransaction.create({ amount: '15000', date: Date.strptime("01/20/2016", "%m/%d/%Y") , transable: cat })

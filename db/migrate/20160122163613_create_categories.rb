class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.string      :source,        null: false, default: ""
      t.string      :desc,          null: false, default: ""
      t.string      :type
      t.references  :user,          index: true, foreign_key: true
      t.timestamps                  null: false
    end
  end
end

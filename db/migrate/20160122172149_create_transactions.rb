class CreateTransactions < ActiveRecord::Migration
  def change
    create_table :transactions do |t|
      t.references    :transable,  index: true, polymorphic: true
      t.integer       :amount
      t.datetime      :date
      t.references    :user,       index: true, foreign_key: true
      t.string        :type
      t.timestamps                 null: false
    end
  end
end
